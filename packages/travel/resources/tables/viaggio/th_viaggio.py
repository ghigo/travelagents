#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('codice')
        r.fieldcell('nome')
        r.fieldcell('descrizione')
        r.fieldcell('operatore_id')
        r.fieldcell('area_geo_codice')
        r.fieldcell('descrizione_estesa')

    def th_order(self):
        return 'codice'

    def th_query(self):
        return dict(column='nome', op='contains', val='')

class ViewFromOperatore(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('codice')
        r.fieldcell('nome')
        r.fieldcell('descrizione')
        r.fieldcell('area_geo_codice')
        r.fieldcell('descrizione_estesa')

    def th_order(self):
        return 'codice'

    def th_query(self):
        return dict(column='nome', op='contains', val='')



class Form(BaseComponent):

    def th_form(self, form):
        tc = form.center.tabContainer()
        fb = tc.contentPane(title='Dati viaggio',datapath='.record').formbuilder(cols=2, border_spacing='4px')
        fb.field('codice')
        fb.field('nome')
        fb.field('operatore_id')
        fb.field('area_geo_codice')
        fb.field('descrizione',colspan=2,width='100%')
        fb.field('caratteristiche_viaggio',tag='checkboxtext',
                table='travel.caratteristiche_viaggio',
                popup=True,colspan=2,width='100%')

        pane = tc.contentPane(title='Dati viaggio',datapath='.record')
        pane.simpleTextArea(value='^.descrizione_estesa',editor=True)


    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px')


class FormFromOperatore(BaseComponent):

    def th_form(self, form):
        tc = form.center.tabContainer()
        fb = tc.contentPane(title='Dati viaggio',datapath='.record').formbuilder(cols=2, border_spacing='4px')
        fb.field('codice')
        fb.field('nome')
        fb.field('area_geo_codice')
        fb.field('descrizione',colspan=2,width='100%')
        fb.field('caratteristiche_viaggio',tag='checkboxtext',
                table='travel.caratteristiche_viaggio',
                popup=True,colspan=2,width='100%')

        pane = tc.contentPane(title='Dati viaggio',datapath='.record')
        pane.simpleTextArea(value='^.descrizione_estesa',editor=True)


    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px')
