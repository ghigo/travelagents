# encoding: utf-8

class Table(object):
    def config_db(self,pkg):
        tbl =  pkg.table('nazione',pkey='codice',name_long='Nazione',name_plural='Nazioni',caption_field='nome',lookup=True)
        self.sysFields(tbl,id=False)
        tbl.column('codice',size=':2',name_long='Codice',unique=True,indexed=True)
        tbl.column('nome',size=':40',name_long='Nome')
