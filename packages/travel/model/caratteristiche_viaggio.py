# encoding: utf-8

class Table(object):
    def config_db(self,pkg):
        tbl =  pkg.table('caratteristiche_viaggio',pkey='id',name_long='Caratteristiche',name_plural='Caratteristiche',caption_field='descrizione',lookup=True)
        self.sysFields(tbl)
        tbl.column('codice',size=':10',name_long='Codice',unique=True,indexed=True)
        tbl.column('descrizione',name_long='Descrizione')
