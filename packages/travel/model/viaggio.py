# encoding: utf-8

class Table(object):
    def config_db(self,pkg):
        tbl =  pkg.table('viaggio',pkey='id',name_long='Viaggio',name_plural='Viaggi',caption_field='nome')
        self.sysFields(tbl)
        tbl.column('codice',size=':20',name_long='Codice',unique=True,indexed=True)
        tbl.column('nome',size=':40',name_long='Nome')
        tbl.column('descrizione',name_long='Descrizione')
        tbl.column('area_geo_codice',size=':6',name_long='Area codice').relation('area_geo.codice',relation_name='viaggi', mode='foreignkey')
        tbl.column('descrizione_estesa',name_long='Descrizione estesa')
        tbl.column('caratteristiche_viaggio',name_long='Caratteristiche')
        tbl.column('operatore_id',size='22',name_long='Operatore').relation('operatore.id',relation_name='viaggi', mode='foreignkey', onDelete='cascade')
