# encoding: utf-8

class Table(object):
    def config_db(self,pkg):
        tbl =  pkg.table('op_specializzazione',pkey='id',name_long='Operatore specializzazione',name_plural='Operatore specializzazioni',caption_field='id')
        self.sysFields(tbl)
        tbl.column('operatore_id',size='22',name_long='Operatore id').relation('operatore.id',relation_name='spec_op', mode='foreignkey', onDelete='cascade')
        tbl.column('specializzazione_codice',size=':6',name_long='Specializzazione codice').relation('specializzazione.codice',relation_name='op_spec', mode='foreignkey', onDelete='cascade')
